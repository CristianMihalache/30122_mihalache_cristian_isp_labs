package isp.mihalache.cristian.Lab7.Exercise4;

import java.io.*;
public class TestCar {
    public static void main(String[] args) {
        Car c = new Car("Audi", 104566);

        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\crist\\Desktop\\ISP\\30122_mihalache_cristian_isp_labs\\ISP2021\\src\\main\\java\\isp\\mihalache\\cristian\\Lab7\\Exercise4\\car.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(c);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved");
        } catch (IOException i) {
            i.printStackTrace();
        }

        Car c1 = null;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\crist\\Desktop\\ISP\\30122_mihalache_cristian_isp_labs\\ISP2021\\src\\main\\java\\isp\\mihalache\\cristian\\Lab7\\Exercise4\\car.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            c1 = (Car) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException ce) {
            System.out.println("Car class not found");
            ce.printStackTrace();
            return;
        }

        System.out.println("\nDeserialized Car...");
        System.out.println("Model: " + c1.getModel());
        System.out.println("Price: " + c1.getPrice());


    }
}

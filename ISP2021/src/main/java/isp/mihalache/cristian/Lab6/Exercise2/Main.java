package isp.mihalache.cristian.Lab6.Exercise2;

public class Main {
    public static void main(String[] args) {
        Bank2 b = new Bank2();
        b.addAccounts("Micky",650);
        b.addAccounts("Jhon",320);
        b.addAccounts("Brad",780);
        b.addAccounts("Harry",122);
        b.addAccounts("Christian",920);

        b.printAccounts();
        System.out.println(".............................");
        b.printAccounts(500,1000);
        System.out.println(".............................");
        System.out.println(b.getAccount("Christian").toString());
        System.out.println(b.getAccount("Micky").toString());
    }

}

package isp.mihalache.cristian.Lab9.Exercise5;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class Simulator extends JFrame {


    JLabel cluj, bucuresti, turceni;
    JLabel seg1, seg2, seg3;  //cluj
    JLabel seg4, seg5, seg6;  //bucuresti
    JLabel seg7, seg8, seg9;  //turceni


    static JTextField tseg1, tseg2, tseg3;
    static JTextField tseg4, tseg5, tseg6;   //bucuresti
    static JTextField tseg7, tseg8, tseg9;   //turceni

    JLabel Lid, Lsegment, Ldestination;
    JTextField id, segment, destination;
    JButton trainAdd;


    Simulator() {
        setTitle("Simulator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(700, 350);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;


        cluj = new JLabel("Cluj");
        cluj.setBounds(100, 10, 100, height);

        seg1 = new JLabel("Segment 1 ");
        seg1.setBounds(10, 30, 100, height);
        seg2 = new JLabel("Segment 2 ");
        seg2.setBounds(10, 50, 100, height);
        seg3 = new JLabel("Segment 3 ");
        seg3.setBounds(10, 70, 100, height);

        tseg1 = new JTextField();
        tseg1.setBounds(80, 30, 100, height);
        tseg2 = new JTextField();
        tseg2.setBounds(80, 50, 100, height);
        tseg3 = new JTextField();
        tseg3.setBounds(80, 70, 100, height);


        bucuresti = new JLabel("Bucuresti");
        bucuresti.setBounds(300, 10, 100, height);

        seg4 = new JLabel("Segment 4 ");
        seg4.setBounds(210, 30, 100, height);
        seg5 = new JLabel("Segment 5 ");
        seg5.setBounds(210, 50, 100, height);
        seg6 = new JLabel("Segment 6 ");
        seg6.setBounds(210, 70, 100, height);

        tseg4 = new JTextField();
        tseg4.setBounds(280, 30, 100, height);
        tseg5 = new JTextField();
        tseg5.setBounds(280, 50, 100, height);
        tseg6 = new JTextField();
        tseg6.setBounds(280, 70, 100, height);

        turceni = new JLabel("Turceni");
        turceni.setBounds(500, 10, 100, height);

        seg7 = new JLabel("Segment 7 ");
        seg7.setBounds(410, 30, 100, height);
        seg8 = new JLabel("Segment 8 ");
        seg8.setBounds(410, 50, 100, height);
        seg9 = new JLabel("Segment 9 ");
        seg9.setBounds(410, 70, 100, height);

        tseg7 = new JTextField();
        tseg7.setBounds(480, 30, 100, height);
        tseg8 = new JTextField();
        tseg8.setBounds(480, 50, 100, height);
        tseg9 = new JTextField();
        tseg9.setBounds(480, 70, 100, height);


        id = new JTextField();
        id.setBounds(10, 150, 200, height);

        segment = new JTextField();
        segment.setBounds(10, 170, 200, height);

        destination = new JTextField();
        destination.setBounds(10, 190, 200, height);

        trainAdd = new JButton("Add new train");
        trainAdd.setBounds(10, 130, 200, height);
        trainAdd.addActionListener(new AddNewTrain());


        //JLabel Lid,Lsegment,Ldestination;


        Lid = new JLabel("ID of Train");
        Lid.setBounds(210, 150, 200, height);

        Lsegment = new JLabel("Segment");
        Lsegment.setBounds(210, 170, 200, height);

        Ldestination = new JLabel("Destination");
        Ldestination.setBounds(210, 190, 200, height);

        add(cluj);
        add(seg1);
        add(seg2);
        add(seg3);
        add(tseg1);
        add(tseg2);
        add(tseg3);

        add(bucuresti);
        add(seg4);
        add(seg5);
        add(seg6);
        add(tseg4);
        add(tseg5);
        add(tseg6);

        add(turceni);
        add(seg7);
        add(seg8);
        add(seg9);
        add(tseg7);
        add(tseg8);
        add(tseg9);


        add(id);
        add(segment);
        add(destination);


        add(Ldestination);
        add(Lid);
        add(Lsegment);


        add(trainAdd);


    }

    class AddNewTrain implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String destination, name;
            destination = Simulator.this.destination.getText();
            name = Simulator.this.id.getText();

            Segment h = new Segment(Integer.parseInt(Simulator.this.segment.getText()));

            Train t = new Train(destination, name);
            h.arriveTrain(t);

            switch (Integer.parseInt(Simulator.this.segment.getText())) {
                case 1:
                    Simulator.tseg1.setText(h.TrainName());
                    break;
                case 2:
                    Simulator.tseg2.setText(h.TrainName());
                    break;
                case 3:
                    Simulator.tseg3.setText(h.TrainName());
                    break;
                case 4:
                    Simulator.tseg4.setText(h.TrainName());
                    break;
                case 5:
                    Simulator.tseg5.setText(h.TrainName());
                    break;
                case 6:
                    Simulator.tseg6.setText(h.TrainName());
                    break;
                case 7:
                    Simulator.tseg7.setText(h.TrainName());
                    break;
                case 8:
                    Simulator.tseg8.setText(h.TrainName());
                    break;
                case 9:
                    Simulator.tseg9.setText(h.TrainName());
                    break;

            }

        }
    }

    public static void main(String[] args) {

        new Simulator();
        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);


        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);


        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build station turceni
        Controler c3 = new Controler("turceni");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);
        //connect the 3 controllers

        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);

        //testing

        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca", "R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Cluj-Napoca", "IR-1358");
        s8.arriveTrain(t3);
        Train t4 = new Train("Turceni", "D-313");
        s2.arriveTrain(t4);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        System.out.println("\nStart train control\n");


        try {
            Simulator.tseg9.setText(s9.TrainName());
        } catch (Exception e1) {
        }

        try {
            Simulator.tseg1.setText(s1.TrainName());
        } catch (Exception e1) {
        }

        try {

        } catch (Exception e1) {
        }
        Simulator.tseg2.setText(s2.TrainName());
        try {

        } catch (Exception e1) {
        }

        try {
            Simulator.tseg3.setText(s3.TrainName());
        } catch (Exception e1) {
        }

        try {
            Simulator.tseg4.setText(s4.TrainName());
        } catch (Exception e1) {
        }

        try {
            Simulator.tseg5.setText(s5.TrainName());
        } catch (Exception e1) {
        }

        try {
            Simulator.tseg6.setText(s6.TrainName());
        } catch (Exception e1) {
        }

        try {
            Simulator.tseg7.setText(s7.TrainName());
        } catch (Exception e1) {
        }

        try {
            Simulator.tseg8.setText(s8.TrainName());
        } catch (Exception e1) {
        }


    }


}

package isp.mihalache.cristian.Lab5.exercise5;

import java.util.Random;

public class TemperatureSensor extends Sensor {

    @Override
    public int readValue() {
        Random random = new Random();
        return random.nextInt(101);
    }


}
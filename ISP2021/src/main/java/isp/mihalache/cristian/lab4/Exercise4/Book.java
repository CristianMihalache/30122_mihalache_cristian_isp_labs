package isp.mihalache.cristian.lab4.Exercise4;
import isp.mihalache.cristian.lab4.Exercise4.Author;
public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;
    private int n = 0;
    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = 0;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;

    }

    public String getName() {
        return name;
    }
    public Author[] getAuthors() {
        return authors;

    }
    public double getPrice() {

        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return ("Book name= " + name + " by "+  authors.length  + " authors:" + ", price= " + price + " In Stoc " + qtyInStock);
    }
    public void printAuthors() {
        System.out.println("The name of the authors are: ");
        int i;
        for (i = 0; i < authors.length; i++)
            System.out.println(authors[i]);

    }

    public static void main(String[] args) {
        Author[] authors = {
                new Author("Robert Black", "theblackrobert@gmail.com", 'M'),
                new Author("Jennifer Johannes", "jjjenifer@gmail.com", 'F'),
                new Author("Austin Swain", "mssw3@gmail.com", 'M')
        };

        Book  book1 = new Book("The name of love", authors, 12.5, 3);
        Book  book2 = new Book("Lonely", authors, 12.5, 1);

        System.out.println(book1);
        System.out.println(book2);
        book2.printAuthors();
    }
}


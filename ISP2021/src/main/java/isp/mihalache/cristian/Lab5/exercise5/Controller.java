package isp.mihalache.cristian.Lab5.exercise5;

public class Controller {
    TemperatureSensor tempSensor;
    LightSensor lightSensor;

    public Controller() {
        tempSensor = new TemperatureSensor();
        lightSensor = new LightSensor();
    }

    public void control() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Temperature sensor value:" + tempSensor.readValue() + " \n Light sensor value:" + lightSensor.readValue());
            System.out.println("----------------------------------------------------");
            sleep();
        }
    }


    public void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

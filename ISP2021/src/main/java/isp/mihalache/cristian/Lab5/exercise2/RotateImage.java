package isp.mihalache.cristian.Lab5.exercise2;

public class RotateImage implements Image {
    private String fileName;

    public RotateImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    public void display() {
        System.out.println("Display rotated " + fileName);
    }

    private void loadFromDisk(String fileName) {
        System.out.println("Loading " + fileName);
    }
}

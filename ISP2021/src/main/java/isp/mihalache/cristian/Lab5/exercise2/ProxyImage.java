package isp.mihalache.cristian.Lab5.exercise2;

public class ProxyImage implements Image{
    private RealImage realImage;
    private RotateImage rotatedImage;
    private String fileName;
    private boolean image;
    public ProxyImage(String fileName,boolean image){
        this.fileName = fileName;
        this.image=image;
    }
    @Override
    public void display() {
        if(this.image){
            realImage = new RealImage(fileName);
            realImage.display();
        }
        else {
            rotatedImage=new RotateImage(fileName);
            rotatedImage.display();
        }
    }
}

package isp.mihalache.cristian.Lab6.Exercise4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleMenu {
    public static void main(String[] args) throws IOException {
        Dictionary d = new Dictionary();
        String line, ex;
        char answer;
        BufferedReader fI = new BufferedReader(new InputStreamReader(System.in));

        do{
            System.out.println("a - Add a word ");
            System.out.println("c - Search for the word ");
            System.out.println("e - Exit");

            line = fI.readLine();
            answer = line.charAt(0);

            switch(answer){
                case 'a': case 'A':
                    System.out.println("Write the word ");
                    line = fI.readLine();
                    if(line.length()>1){
                        System.out.println("Set the definition ");
                        ex = fI.readLine();
                        d.addWord(new Word(line),new Definition(ex));
                    }
                    break;

                case 'c': case 'C':
                    System.out.println("Searched word ");
                    line = fI.readLine();
                    if(line.length()>1){
                        System.out.println(d.getDefinition(new Word(line)));
                    }
            }

        }while(answer != 'e'&& answer != 'E');
    }
}

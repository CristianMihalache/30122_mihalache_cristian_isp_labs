package isp.mihalache.cristian.Lab5.exercise3;

import java.util.Random;

public class LightSensor extends Sensor {
    public int readValue() {
        Random number = new Random();
        return number.nextInt(100);
    }
}

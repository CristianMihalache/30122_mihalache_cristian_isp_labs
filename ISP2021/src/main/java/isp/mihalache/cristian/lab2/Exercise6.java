package isp.mihalache.cristian.lab2;

public class Exercise6 {
    public static int nonRecursive(int n){
        int factorial=1;
        for(int i=1 ;i<=n;i++) {
            factorial*=i;
        }
    return factorial;
    }
    public static int Recursive(int x){
        if(x==1) {
            return x;
        }
        else {
            return x*Recursive(x-1);
        }
    }
    public static void main(String[] args) {
        System.out.println("The N! is " + nonRecursive(5));
        System.out.println("The N! recursive is " + Recursive(5));
    }
}

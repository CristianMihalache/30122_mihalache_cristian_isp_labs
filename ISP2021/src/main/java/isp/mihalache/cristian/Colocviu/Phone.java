package isp.mihalache.cristian.Colocviu;

public class Phone extends PhoneService {
 String brand;
 String batteryProducer;
 boolean isSmartphone;
 double battery;
 Phone(String brand, String batteryProducer, boolean isSmartphone, double battery){
    this.brand=brand;
    this.batteryProducer=batteryProducer;
    this.isSmartphone=isSmartphone;
    this.battery=battery;
 }
    public String getBrand() {
        return brand;
    }

    public String getBatteryProducer() {
        return batteryProducer;
    }

    public boolean isSmartphone() {
        return isSmartphone;
    }

    public double getBattery() {
        return battery;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public void setBatteryProducer(String batteryProducer) {
        this.batteryProducer = batteryProducer;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setIsSmartphone(boolean smartphone) {
        isSmartphone = smartphone;
    }
    public String toString() {
        return "Phone{" +
                "brand=" + brand +
                ", batter producer='" + batteryProducer + '\'' +
                ", isSmartphone='" + isSmartphone + '\'' +
                ", battery=" + battery +
                '}';
    }
}

package isp.mihalache.cristian.Lab6.Exercise1;

import java.util.Objects;

public class BankAccount {
    String owner;
    double balance;
    BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void withdraw(double amount){
    if(amount>balance) {
        System.out.println("The amount of money that you are trying to withdraw is too big.");
    }
    else{
        balance=balance-amount;
    }
    }
    public void deposit(double amount){
        balance= balance+amount;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
    @Override
    public String toString() {
        return "BankAccount owned by " + owner + " with a balance of " + balance +" euros";
    }

}


package isp.mihalache.cristian.Lab5.exercise4;


import isp.mihalache.cristian.Lab5.exercise3.LightSensor;
import isp.mihalache.cristian.Lab5.exercise3.TemperatureSensor;

public class Singleton {
    public static volatile Singleton instance = null;

    public Singleton() {

    }

    public static Singleton getInstance() {
        synchronized (Singleton.class) {
            if (instance == null) {
                instance = new Singleton();
            }
        }
        return instance;
    }

    LightSensor lightSens = new LightSensor();
    TemperatureSensor tempSens = new TemperatureSensor();

    public void control() {
        int i = 0;
        while (i < 10) {
            System.out.println("Temperature: " + tempSens.readValue());
            System.out.println("Light: " + lightSens.readValue());

            try {
                Thread.sleep(1000);
                System.out.println("-------------");
            } catch (InterruptedException e) {
                System.err.println(e);
            }
            i++;
        }
    }

    public static void main(String[] args) {
        Singleton singletonController = new Singleton();
        singletonController.control();
    }

}

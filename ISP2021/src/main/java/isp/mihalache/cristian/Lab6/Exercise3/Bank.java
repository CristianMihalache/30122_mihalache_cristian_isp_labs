package isp.mihalache.cristian.Lab6.Exercise3;


import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

public class Bank {

    private TreeSet<BankAccount> accounts= new TreeSet<BankAccount>();


    public void addAccount(String owner,double balance){

        accounts.add(new BankAccount(owner,balance));

    }
    public void printAccounts(){

        System.out.println(Arrays.toString(accounts.toArray()));
    }

    public void printAccounts(double minBalance,double maxBalance){

        System.out.println( "Bankaccounts between "+minBalance+" and "+maxBalance);
        Iterator<BankAccount> itr = accounts.iterator();
        BankAccount aux;
        while (itr.hasNext()) {
            aux=itr.next();
            if((aux.getBalance()>=minBalance)&&(aux.getBalance()<=maxBalance));
            System.out.println(aux);
        }

    }

    public BankAccount getAccount(String owner) {

        System.out.println("Searched by Owner:");
        Iterator<BankAccount> itr = accounts.iterator();
        BankAccount aux;
        while (itr.hasNext()) {
            aux = itr.next();
            if(aux.getOwner()==owner)
                return aux;
        }
        return null;
    }

}
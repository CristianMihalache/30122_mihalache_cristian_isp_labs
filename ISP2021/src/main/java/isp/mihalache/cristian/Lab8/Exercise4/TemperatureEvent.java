package isp.mihalache.cristian.Lab8.Exercise4;

public class TemperatureEvent extends Event {
    private int value;

    TemperatureEvent(int value) {
        super(EventType.FIRE.TEMPERATURE);
        this.value = value;
    }

    int getVlaue() {
        return value;
    }

    @Override
    public String toString() {
        if (getVlaue() > 23)
            return "TemperatureEvent " + "value=" + value + " " + new CoolingUnit().toString();
        else
            return "TemperatureEvent " + "value=" + value + " " + new HeatingUnit().toString();
    }
}

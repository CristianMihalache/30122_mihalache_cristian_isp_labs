package isp.mihalache.cristian.Lab5.exercise2;



public class TestImage {
    public static void main(String[] args) {
        ProxyImage proxy = new ProxyImage("Picture", false);
        ProxyImage proxy2 = new ProxyImage("Picture2", true);
        proxy.display();
        System.out.println("----------");
        proxy2.display();
    }
}

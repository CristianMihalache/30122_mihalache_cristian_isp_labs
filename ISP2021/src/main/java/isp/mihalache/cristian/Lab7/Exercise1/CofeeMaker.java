package isp.mihalache.cristian.Lab7.Exercise1;

public class CofeeMaker{
    static int coffeeCount = 0;
    int LIMIT = 5;

    Coffee makeCofee() throws MakeException {
        if (coffeeCount >= LIMIT) {
            throw (new MakeException("You made too much coffee :'( "));
        }
        System.out.println("Make a coffee!");
        coffeeCount++;
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Coffee coffee = new Coffee(t, c);
        return coffee;
    }
}

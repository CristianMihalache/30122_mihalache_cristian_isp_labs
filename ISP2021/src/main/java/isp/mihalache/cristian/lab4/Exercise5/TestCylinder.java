package isp.mihalache.cristian.lab4.Exercise5;

public class TestCylinder {
        public static void main(String[] args) {
            Cylinder c = new Cylinder(3, 7);
            System.out.println(" Radius of Cylinder is: " + c.getRadius() + "\n Height of Cylinder is: " + c.getHeight() + "\n Volume of Cylinder is: " + c.getVolume() + "\n Area of Cylinder is: " + c.getArea());
        }
}

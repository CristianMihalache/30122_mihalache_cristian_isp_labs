package isp.mihalache.cristian.lab4.Exercise5;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {

    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        double v = 2 * Math.PI * getRadius() * getRadius() + getHeight() * (2 * Math.PI * getRadius());
        return v;
    }

    @Override
    public double getArea() {
        return super.getArea();
    }
}
package isp.mihalache.cristian.Lab8.Exercise3;

import java.util.*;

public class Controler {
    String stationName;

    ArrayList<Controler> neighbourController = new ArrayList<Controler>();


    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v) {
        neighbourController.add(v);
    }

    void addControlledSegment(Segment s) {
        list.add(s);
    }

    int getFreeSegmentId() {
        for (Segment s : list) {
            if (s.hasTrain() == false)
                return s.id;
        }
        return -1;
    }

    void controlStep() {

        for (Controler controler : neighbourController) {
            for (Segment segment : list) {
                if (segment.hasTrain()) {
                    Train t = segment.getTrain();
                    if (t.getDestination().equals(controler.stationName)) {

                        int id = controler.getFreeSegmentId();
                        if (id == -1) {
                            System.out.println("The train +" + t.name + "from trainstation" + stationName + " can not be transmitted to" + controler.stationName + ". No segment!");
                            return;
                        }
                        //send train
                        System.out.println("The Train" + t.name + " goes from " + stationName + " to trainstation " + controler.stationName);
                        segment.departTRain();
                        controler.arriveTrain(t, id);
                    }

                }

            }
        }

    }


    public void arriveTrain(Train t, int idSegment) {
        for (Segment segment : list) {

            if (segment.id == idSegment)
                if (segment.hasTrain() == true) {
                    System.out.println("CRASH! Train " + t.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName);
                    return;
                } else {
                    System.out.println("Train " + t.name + " arrived on segment " + segment.id + " in station " + stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        System.out.println("Train " + t.name + " cannot be received " + stationName + ". Check controller logic algorithm!");

    }


    public void displayStationState() {
        System.out.println("|=== STATION " + stationName + " ===|");
        for (Segment s : list) {
            if (s.hasTrain())
                System.out.println("ID=" + s.id + " Train=" + s.getTrain().name + " to " + s.getTrain().destination + "");
            else
                System.out.println("ID=" + s.id + " Train");
        }
    }
}

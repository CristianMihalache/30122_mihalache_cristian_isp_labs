package isp.mihalache.cristian.Lab10.Exercise6;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class Controller implements ActionListener, Observer {

     CThread cthread;
    Chronometer crn;

    public Controller(CThread c, Chronometer crn) {
        this.cthread = c;
        this.crn = crn;
        crn.getStartButton().addActionListener(this);
        crn.getReset().addActionListener(this);
        cthread.addObserver(this);
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        crn.getTextF().setText(cthread.h + ":" + cthread.min + ":" +
                cthread.sec + ":" + cthread.milisec);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(crn.getStartButton())) {

            if (cthread.startButon) {
                cthread.changeButtonState();
                crn.getStartButton().setText("Start");
            } else {
                crn.getStartButton().setText("Stop");
                cthread.changeButtonState();

            }

        } else if (e.getSource().equals(crn.getReset())) {
            cthread.milisec = 0;
            cthread.sec = 0;
            cthread.min = 0;
            cthread.h = 0;
            crn.getTextF().setText("0:0:0:0");
        }

    }

}



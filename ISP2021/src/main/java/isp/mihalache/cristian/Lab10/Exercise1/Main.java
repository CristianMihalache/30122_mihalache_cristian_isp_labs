package isp.mihalache.cristian.Lab10.Exercise1;

public class Main {
    public static void main(String[] args) {
        Counter c1 = new Counter("counter1");
        Counter c2 = new Counter("counter2");
        Counter c3 = new Counter("counter3");

        /**
         * in cazul metodei start toate Thread-urile
         * functioneaza deodata si se numara in acelasi timp
         * pe toate counterele
         */
        c1.start();
        c2.start();
        c3.start();

        /**
         * in cazut metodei run, fiecare counter numara pe rand
         * asteptanduse o valoare random intre 0 si 1000ms
         * pana sa se printeze urmatoarea valoare
         */
        c1.run();
        c2.run();
        c3.run();
    }
}

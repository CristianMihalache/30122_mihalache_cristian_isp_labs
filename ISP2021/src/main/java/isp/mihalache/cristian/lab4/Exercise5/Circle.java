package isp.mihalache.cristian.lab4.Exercise5;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    Circle(String color, double r) {
        this.color = color;
        this.radius = r;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea() {

        return Math.PI * this.radius * this.radius;
    }

    public String getColor() {

        return this.color;
    }
}
package isp.mihalache.cristian.Lab5.exercise3;

public class Controller {
    TemperatureSensor tempSens;
    LightSensor lightSens;

    public Controller(TemperatureSensor tempSens, LightSensor lightSens) {
        this.lightSens = lightSens;
        this.tempSens = tempSens;
    }

    public void control() throws InterruptedException {
        System.out.println(tempSens.readValue() + " " + lightSens.readValue());
        Thread.sleep(1000);

    }
}

package isp.mihalache.cristian.Lab9.Exercise3;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;


public class OpenFile extends JFrame {
    JTextField file;
    JTextArea content;
    JButton button;

    OpenFile() {
        setTitle("File opener");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 300);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        file = new JTextField();
        file.setBounds(10, 10, width, height);

        button = new JButton("Import");
        button.setBounds(10, 40, width, height);
        button.addActionListener(new ImportFile());

        content = new JTextArea();
        content.setBounds(10, 70, 400, 300);

        add(file);
        add(button);
        add(content);
    }

    public static void main(String[] args) {
        new OpenFile();
    }

    class ImportFile implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                content.setText("");
                BufferedReader bf =
                        new BufferedReader(
                                new FileReader("C:\\Users\\crist\\Desktop\\ISP\\30122_mihalache_cristian_isp_labs\\ISP2021\\src\\main\\java\\isp\\mihalache\\cristian\\Lab9\\Exercise3\\" + OpenFile.this.file.getText() + ".txt"));
                String l = "";
                l = bf.readLine();
                while (l != null) {
                    content.append(l + "\n");
                    l = bf.readLine();
                }
            } catch (Exception e1) {
            }
        }
    }

}
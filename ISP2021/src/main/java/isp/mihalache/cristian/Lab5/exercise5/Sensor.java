package isp.mihalache.cristian.Lab5.exercise5;

public abstract class Sensor {
    private String location;

    public abstract int readValue();

    public String getLocation() {
        return this.location;
    }
}


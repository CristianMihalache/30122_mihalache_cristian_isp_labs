package isp.mihalache.cristian.lab3.Exercise1;

public class Robot {
    int x; //The position of the robot

    public Robot(){

        this.x=1;
    }

    public void change(int k){
        if(k>=1){
            this.x=k;
        }
    }

    public String toString(){

        return "The position of the Robot is: "+this.x;
    }
}

package isp.mihalache.cristian.Lab8.Exercise3;

import java.util.*;

public class Simulator {

    public static void main(String[] args) {


        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);


        Controler c2 = new Controler("Turceni");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        Controler c3 = new Controler("Bucuresti");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);
        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        //connect the 3 controllers

        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);



        Train t1 = new Train("Bucuresti", "IR-1322");
        s1.arriveTrain(t1);
        Train t10 = new Train("Turceni", "IR-3185");
        s2.arriveTrain(t10);
        Train t2 = new Train("Cluj-Napoca", "R-002");
        s5.arriveTrain(t2);
        Train t9 = new Train("Turceni", "Rv-1323");
        s6.arriveTrain(t9);
        Train t3 = new Train("Bucuresti", "BC-965");
        s8.arriveTrain(t3);
        Train t4 = new Train("Cluj-Napoca", "IR-1545");
        s7.arriveTrain(t4);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();
        System.out.println("\nStart train control\n");


        for (int i = 0; i < 3; i++) {
            System.out.println("### Step " + i + " ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }
}

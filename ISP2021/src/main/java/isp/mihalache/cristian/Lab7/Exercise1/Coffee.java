package isp.mihalache.cristian.Lab7.Exercise1;

public class Coffee {
    private int temp;
    private int conc;

    Coffee(int t, int c) {
        temp = t;
        conc= c;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    @Override
    public String toString() {
        return "[Coffee temperature=" + temp + ":concentration=" + conc + "]";
    }
}

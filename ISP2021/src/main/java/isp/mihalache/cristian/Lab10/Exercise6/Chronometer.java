package isp.mihalache.cristian.Lab10.Exercise6;

import javax.swing.*;

public class Chronometer extends JFrame {

    JButton startButton;
    JButton reset;
    JTextField textF;


    Chronometer() {
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initializare();
        setSize(500, 500);
        setVisible(true);
    }

    public void initializare() {
        this.setLayout(null);

        int width = 100;
        int height = 50;

        textF = new JTextField();
        textF.setBounds(175, 100, width, 30);
        textF.setEditable(false);

        startButton = new JButton("START");
        startButton.setBounds(100, 170, width, height);

        reset = new JButton("RESET");
        reset.setBounds(250, 170, width, height);

        add(textF);
        add(startButton);
        add(reset);

    }

    public JButton getStartButton() {
        return startButton;
    }

    public void setStartButton(JButton starop) {
        this.startButton = starop;
    }

    public JButton getReset() {
        return reset;
    }

    public void setReset(JButton reset) {
        this.reset = reset;
    }

    public JTextField getTextF() {
        return textF;
    }

    public void setTextF(JTextField textF) {
        this.textF = textF;
    }


}


package isp.mihalache.cristian.Lab9.Exercise2;

import javax.swing.*;
import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class ButtonApp extends JFrame {
    JTextField tCounter;
    JButton bCounter;


    ButtonApp() {
        setTitle("CounterApp");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 200);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        bCounter = new JButton("Count");
        bCounter.setBounds(100, 100, width, height);

        tCounter = new JTextField("0", 4);
        tCounter.setBounds(100, 50, width, height);

        bCounter.addActionListener(new TratareButonCount());


        add(bCounter);
        add(tCounter);


    }

    public static void main(String[] args) {
        new ButtonApp();
    }

    class TratareButonCount implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String value = ButtonApp.this.tCounter.getText();
            int counter = Integer.parseInt(value);

            counter++;

            ButtonApp.this.tCounter.setText(String.valueOf(counter));

        }
    }


}


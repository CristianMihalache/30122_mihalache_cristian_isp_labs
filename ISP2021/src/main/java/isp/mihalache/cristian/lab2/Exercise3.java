package isp.mihalache.cristian.lab2;

import java.util.Scanner;

public class Exercise3 {
    public static boolean isPrime(int n)
    {
        if(n<2) {
            return false;
        }
        else if(n==2) {
            return true;
        }
        else if(n%2==0) {
            return false;
        }
        else {
            for(int i=3; i*i<= n; i+=2) {
                if(n%i==0)
                    return false;
            }

        }
        return true;
    }
    public static void Prime() {
        Scanner in= new Scanner(System.in);
        int A= in.nextInt();
        int B= in.nextInt();
        int NrPrime=0;
        for(int i=A;i<=B;i++) {
            if(isPrime(i)==true) {
                System.out.println(i);
                NrPrime++;

            }
        }
        System.out.println("There are "+ NrPrime + " prime numbers");
    }

    public static void main(String[] args) {
        Prime();
    }
}
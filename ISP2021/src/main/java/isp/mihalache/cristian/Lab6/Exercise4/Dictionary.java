package isp.mihalache.cristian.Lab6.Exercise4;

import java.util.HashMap;

public class Dictionary {

    private HashMap<Word, Definition> dictionary = new HashMap<Word, Definition>();

    public void addWord(Word w,Definition d){

        dictionary.put(w,d);
    }

    public Definition getDefinition(Word w){

        return dictionary.get(w);
    }

    public void getAllWords(){

        for(Word index : dictionary.keySet())
            System.out.println(index.toString());
    }

    public void getAllDefinition(){

        for(Definition index : dictionary.values())
            System.out.println(index.toString());
    }
}

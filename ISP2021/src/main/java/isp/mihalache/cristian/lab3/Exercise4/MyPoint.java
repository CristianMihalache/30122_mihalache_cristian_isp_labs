package isp.mihalache.cristian.lab3.Exercise4;

import java.util.Scanner;

public class MyPoint {
    int x;
    int y;
    MyPoint(){
        this.x=0;
        this.y=0;
    }

    public MyPoint(int x,int y) {
        this.x = x;
        this.y = y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String ToString(){
        return "(" +this.x+ ", " +this.y+ ")";
    }
    public  double distance(int x, int y) {
        double d;
        return d = Math.sqrt((this.x - x) * (this.x - x) + (this.y - y) * (this.y - y));
    }
    public double distance(MyPoint another){
        int x1 = this.x - another.x;
        int y1 = this.y - another.y;
        return Math.sqrt(x1 * x1 + y1 * y1);
    }

}

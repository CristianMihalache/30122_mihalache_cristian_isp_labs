package isp.mihalache.cristian.Lab10.Exercise3;

public class WaitingCounters extends Thread {

    public int start;
    public int end;

    WaitingCounters(String name, int start, int end) {
        super(name);
        this.start = start;
        this.end = end;
    }


    public void run() {
        for (int i = start; i < end; i++) {
            System.out.println(getName() + " i = ----- " + i);
            try {
                Thread.sleep((int) (Math.random() * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Job finalised for counter " + getName());
        System.out.println("Next counter will start soon");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        WaitingCounters c1 = new WaitingCounters("counter1", 0, 101);
        WaitingCounters c2 = new WaitingCounters("counter2", 101, 201);

        c1.run();
        c2.run();
    }
}

package isp.mihalache.cristian.Lab6.Exercise2;

import java.util.ArrayList;

public class Bank2 {

    BankAccount bankAccount;
    ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccounts(String owner, double balance){

        bankAccount = new BankAccount(owner, balance);
        accounts.add(bankAccount);
    }

    public void printAccounts(){

        int n = accounts.size();
        for(int i=0;i<n;i++){
            for(int j=i+1;j<n;j++){
                if(accounts.get(i).getBalance() > accounts.get(j).getBalance()){
                    BankAccount aux;
                    aux = accounts.get(i);
                    accounts.set(i,accounts.get(j));
                    accounts.set(j,aux);
                }
            }
        }
        for(BankAccount index : accounts){
            System.out.println(index.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){

        for(BankAccount index : accounts){
            if(index.getBalance() >= minBalance && index.getBalance() <= maxBalance){
                System.out.println(index.toString());
            }
        }
    }

    public BankAccount getAccount(String owner){

        BankAccount account;
        for(BankAccount index : accounts) {
            account = new BankAccount(index.getOwner(),index.getBalance());
            if(index.getOwner().equals(owner))
                return account;
        }
        return null;
    }

}

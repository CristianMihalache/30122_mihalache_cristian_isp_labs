package isp.mihalache.cristian.Lab6.Exercise3;

public class BankAccount implements Comparable<BankAccount>{

    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){

        this.owner = owner;
        if(balance > 0) {
            this.balance = balance;
        }
    }

    public double getBalance() {

        return balance;
    }

    public String getOwner() {

        return owner;
    }

    public void deposit(double amount){

        this.balance += amount;
    }

    public String toString() {

        return this.owner + " " + this.balance;
    }

    public void witdraw(double amount){

        this.balance -=amount;
    }

    @Override
    public int compareTo(BankAccount o) {
        return 0;
    }
}

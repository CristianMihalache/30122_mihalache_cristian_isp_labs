package isp.mihalache.cristian.Lab11.Exercise2;

public class StockController {
    Product p;
    UserInterface view;

    public StockController(Product p, UserInterface view) {
        p.addObserver(view);
        this.p = p;
        this.view = view;
    }

    public static void main(String[] args) {
        UserInterface view = new UserInterface();
        Product p = new Product();
        StockController stockController = new StockController(p, view);
    }
}

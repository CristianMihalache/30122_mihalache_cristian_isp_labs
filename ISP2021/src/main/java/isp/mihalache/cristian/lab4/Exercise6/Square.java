package isp.mihalache.cristian.lab4.Exercise6;

public class Square extends Rectangle {
    public Square() {
        super();
    }

    public Square(double side) {
        super();
        super.setLength(side);
        super.setWidth(side);
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    @Override
    public void setWidth(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return " A Square with side = " + super.getLength() + " which is a subclass of " + super.toString();
    }
}

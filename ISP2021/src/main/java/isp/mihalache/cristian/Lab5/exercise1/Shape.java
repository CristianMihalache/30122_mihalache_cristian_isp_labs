package isp.mihalache.cristian.Lab5.exercise1;

public abstract class Shape {
    String color;
    boolean filled;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        return " A Shape with color of " + this.color + " and " + ((this.filled) ? " filled " : " not filled. ");
    }

    public abstract double getArea();

    public abstract double getPerimeter();

}

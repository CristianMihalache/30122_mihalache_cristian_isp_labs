package isp.mihalache.cristian.Lab5.exercise3;

public abstract class Sensor {
    String location;
    public abstract int readValue();

    public String getLocation() {
        return location;
    }
}

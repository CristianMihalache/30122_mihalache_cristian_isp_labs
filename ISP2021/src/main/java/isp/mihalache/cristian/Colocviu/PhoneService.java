package isp.mihalache.cristian.Colocviu;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class PhoneService {
    private ArrayList<Phone> phones = new ArrayList<>();
    private ArrayList <Phone> phonesSorted = new ArrayList<>();
    private ArrayList <Phone> phonestoSerialize= new ArrayList<>();
    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public void updateBattery(String brand, double battery) {
        for (Phone phone : phones) {
            if (phone.getBrand().equals(brand)) {
                phone.setBattery(battery);
            }
        }
    }

    public ArrayList<Phone> getPhonesByBrandOrderedByBatteryProducer(String brand) {
        for (Phone phone : phones) {
            if (phone.getBrand() == brand) {
                phonesSorted.add(phone);
            }
            Collections.sort(phonesSorted);
        }
    return phonesSorted;
    }

    public void serializePhonesByIsSmarphone(){
        for (Phone phone : phones) {
            if (phone.isSmartphone == true) {
                try {
                    FileOutputStream fileOut =
                            new FileOutputStream("C:\\Users\\crist\\Desktop\\ISP\\30122_mihalache_cristian_isp_labs\\ISP2021\\src\\main\\java\\isp\\mihalache\\cristian\\Colocviu\\phone.abc");
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(phone);
                    out.close();
                    fileOut.close();
                    System.out.printf("Serialized data is saved");
                } catch (IOException i) {
                    i.printStackTrace();
                }
            }
        }
    }
    public ArrayList<Phone> loadSerializedPhones() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("smartphone.abc"));
            return (ArrayList<Phone>) in.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}

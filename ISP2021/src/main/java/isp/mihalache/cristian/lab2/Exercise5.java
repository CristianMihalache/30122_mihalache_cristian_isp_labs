package isp.mihalache.cristian.lab2;

import java.util.Scanner;

public class Exercise5 {
    public static void primeNumber() {
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        for (int number = A; number <= B; number++) {
            boolean prime = false;
            for (int i = 2; i <= number / 2; ++i) {

                if (number % i == 0) {
                    prime = true;
                    break;
                }

            }

            if (!prime) System.out.println(number);
        }
    }
    public static void main(String[] args) {

        primeNumber();
    }
}

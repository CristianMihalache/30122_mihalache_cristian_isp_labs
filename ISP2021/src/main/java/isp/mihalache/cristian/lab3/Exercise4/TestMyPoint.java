package isp.mihalache.cristian.lab3.Exercise4;



public class TestMyPoint {
    public static void main(String[] args) {

        int x,y;
        MyPoint Point1= new MyPoint(2,7);
        System.out.println("Coordinates are: x="+Point1.getX()+" y="+ Point1.getY());

        Point1.setX(8);
        Point1.setY(12);

        System.out.println("Coordinates are: x="+Point1.getX()+" y="+ Point1.getY());

        Point1.setXY(14,21);

        System.out.println("Coordinates are: x="+Point1.getX()+" y="+ Point1.getY());

        System.out.println(Point1.toString());

        System.out.println("Distance:" + Point1.distance(7,2));

    }
}

package isp.mihalache.cristian.lab4.Exercise3;

import isp.mihalache.cristian.lab4.Exercise3.Author;

public class TestBook {
    public static void main(String[] args) {

        Author Author1 = new Author("Robert Black", "theblackrobert@gmail.com", 'M');
        Author Author2 = new Author("Jennifer Johannes", "jjjenifer@gmail.com", 'F');
        Book book1 = new Book("The name of love",Author1,12.5);
        Book book2 = new Book("Lonely",Author2,12.5,3);
        System.out.println(book1.toString());
        System.out.println(book2.toString());

    }
}

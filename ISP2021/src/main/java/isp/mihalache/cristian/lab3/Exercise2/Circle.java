package isp.mihalache.cristian.lab3.Exercise2;

public class Circle {
    private double radius = 1.0;
    private String color = "red";
    Circle(String color, double r){
        this.color = color;
        this.radius = r;
    }
    public double getRadius(){
        System.out.println("The radius of the Circle is: ");
        return this.radius;
    }

    public double getArea(){
        System.out.println("The Area of the Circle is: ");
        return Math.PI * this.radius * this.radius;
    }

    public String getColor(){
        System.out.println("The color of the Circle is: ");
        return this.color;
    }


}

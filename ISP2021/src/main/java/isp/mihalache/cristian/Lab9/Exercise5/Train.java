package isp.mihalache.cristian.Lab9.Exercise5;


public class Train {
    String destination;
    String name;

    public Train(String destination, String name) {
        super();
        this.destination = destination;
        this.name = name;
    }

    String getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        return this.name;
    }
}

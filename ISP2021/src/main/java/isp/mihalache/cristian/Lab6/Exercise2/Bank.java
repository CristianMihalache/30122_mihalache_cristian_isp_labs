package isp.mihalache.cristian.Lab6.Exercise2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Bank implements Comparator<BankAccount> {
    BankAccount b;
    ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccounts(String owner, double balance){
        b = new BankAccount(owner,balance);
        accounts.add(b);
    }

    public ArrayList<BankAccount> getAccounts(){
        return accounts;
    }

    public void printConturi(){

        //Collections.sort(accounts);
        System.out.println(accounts);
    }

    public void printAccounts(double minBalance, double maxBalance){
        for(BankAccount index : accounts){
            if(index.getBalance() > minBalance && index.getBalance() <= maxBalance){
                System.out.println(index.toString());
            }
        }
    }

    public BankAccount getAccount(String owner){
        BankAccount account;
        for(BankAccount index : accounts){
            account = new BankAccount(index.getOwner(), index.getBalance());
            if(index.getOwner().equals(owner))
                return account;
        }
        return null;
    }

    public void getAllAccounts(){
        Collections.sort(accounts, new Bank());
        for(BankAccount index : accounts)
            System.out.println(index);
    }

    @Override
    public int compare(BankAccount c1, BankAccount c2){
        return c1.getOwner().compareTo(c2.getOwner());
    };
}

package isp.mihalache.cristian.Lab7.Exercise2;
import java.io.*;

public class CharacterCounter {
    public static void main(String[] args) throws IOException{
        char character;

        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("Give the character to look for:");
        character = stdin.readLine().charAt(0);

        int number = 0;
        BufferedReader in = new BufferedReader(
                new FileReader("C:\\Users\\crist\\Desktop\\ISP\\30122_mihalache_cristian_isp_labs\\ISP2021\\src\\main\\java\\isp\\mihalache\\cristian\\Lab7\\Exercise2\\data.txt"));
        String s = new String();
        while ((s = in.readLine()) != null) {
            char text[] = s.toCharArray();
            for (int i = 0; i < text.length; i++) {
                if (text[i] == character) {
                    number++;
                }
            }
        }
        in.close();
        System.out.println("The character '" + character + "' appears in the phrases " + number + " times");
    }

}

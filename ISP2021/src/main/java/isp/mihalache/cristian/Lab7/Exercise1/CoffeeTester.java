package isp.mihalache.cristian.Lab7.Exercise1;

public class CoffeeTester {
    public static void main(String[] args) {
        CofeeMaker make = new CofeeMaker();
        CoffeeDrinker drinker1 = new CoffeeDrinker();

        for (int i = 0; i < 7; i++) {

            try {
                Coffee c = make.makeCofee();

                try {
                    drinker1.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
                } finally {
                    System.out.println("Throw the cofee cup.\n");
                }
            } catch (MakeException e) {
                System.out.println("Exception:" + e.getMessage());
            }
        }
    }
}

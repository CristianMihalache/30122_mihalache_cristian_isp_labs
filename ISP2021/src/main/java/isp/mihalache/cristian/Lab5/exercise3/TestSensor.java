package isp.mihalache.cristian.Lab5.exercise3;

public class TestSensor {
    public static void main(String[] args) throws InterruptedException {
        TemperatureSensor tempSens = new TemperatureSensor();
        LightSensor lightSens = new LightSensor();

        tempSens.getLocation();
        lightSens.getLocation();

        System.out.println(tempSens.readValue());
        System.out.println(lightSens.readValue());

        System.out.println("\n Testing the controller: ");

        for (int i = 0; i < 7; i++) {
            Controller c = new Controller(tempSens, lightSens);
            c.control();
            Thread.sleep(1000);
        }
    }
}

package isp.mihalache.cristian.lab3.Exercise3;

public class Author {
    private String name;
    private String email;
    private final char gender;
    public Author(String name, String email,char gender)
    {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    public String getName(){
        return name;
    }

    public String getEmail(){
        return email;
    }

    public char getGender(){
        return gender;
    }


    public String toString(){
        return ("The name of the Author is " +name +", their gender is " +gender + ", you can contact them at "+email);
    }
}


package isp.mihalache.cristian.Lab6.Exercise4;

public class Definition {
    private String description;

    public Definition(String description){

        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return this.getDescription();
    }
}

package isp.mihalache.cristian.Lab6.Exercise3;

public class Main {
    public static void main(String[] args) {

        Bank b = new Bank();

        b.addAccount("Mihaly", 460);
        b.addAccount("Sendy", 350);
        b.addAccount("Salony", 370);
        b.addAccount("Gregor", 670);
        b.addAccount("Alexander", 380);
        b.printAccounts();
        b.printAccounts(300,600);
        b.getAccount("Mihaly");
    }
}

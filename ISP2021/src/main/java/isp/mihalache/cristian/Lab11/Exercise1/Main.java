package isp.mihalache.cristian.Lab11.Exercise1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class Main extends JFrame implements Observer {
    JTextField text;
    JLabel label;
    JButton active, pause;
    static boolean state = false;
    boolean paused = false;
    SensorMonitor sensor = new SensorMonitor(this);


    Main() {
        setTitle("Sensor Monitoring Application");

        setLocation(800, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(210, 200);
        setVisible(true);
    }

    void init() {
        this.setLayout(null);
        sensor.addObserver(this);
        label = new JLabel("Temperature:");
        label.setBounds(20, 20, 150, 20);
        text = new JTextField();
        text.setEditable(false);
        text.setBounds(100, 20, 50, 20);
        active = new JButton("Activate");
        active.setBounds(20, 60, 150, 20);
        pause = new JButton("Pause");
        pause.setBounds(20, 100, 150, 20);
        active.addActionListener(new Active());
        pause.addActionListener(new Pause());
        add(pause);
        add(active);
        add(label);
        add(text);
    }

    class Active implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (state) {
                state = false;

            } else {
                state = true;
                active.setText("Stop");
            }
        }
    }

    class Pause implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (paused) {
                paused = false;
                pause.setText("Pause");
            } else {
                pause.setText("Resume");
                paused = true;
            }
            sensor.setPause(paused);
        }
    }

    public void update(Observable o, Object arg) {
        String s = "" + ((SensorMonitor) o).getTemperature();
        text.setText(s);
    }

    public static void main(String[] args) {
        Main u = new Main();
        while (!state) {
            System.out.println(state);
        }
        u.sensor.start();

    }
}
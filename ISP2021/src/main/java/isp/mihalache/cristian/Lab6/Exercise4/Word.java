package isp.mihalache.cristian.Lab6.Exercise4;

public class Word {

    private String name;

    public Word(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Word))
            return false;
        Word w = (Word) o;
        return name.equals(w.name);
    }

    public int hashCode(){
        return (int)(name.length()*100);
    }
}
